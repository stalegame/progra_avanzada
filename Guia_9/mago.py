from fighter import Fighter

class Wizards(Fighter):

    def __init__(self):
        self.__name = "Mago Daño explosivo"
        self.__hp = 80
        self.__damage = None
        self.__spellOk = False


    @property
    def name(self):
        return self.__name

    @property
    def hp(self):
        return self.__hp

    @property
    def damage(self):
        return self.__damage

    #Muestra el tipo de objeto que es
    def toString(self):
        return 'Este objeto es de tipo <Wizards>'

    #Hace que el False, pase a True
    def prepareSpell(self):
        self.__spellOk = not self.__spellOk

    #Dependiendo de si crea o no el hechizo, muestra si sera vulnerable
    def isVulnerable(self):
        if self.__spellOk:
            return False
        else:
            return True

    #Dependiendo de si crea el hechizo hace cierta cantidad de daño
    def damagePoints(self):
        if self.__spellOk:
            return 12
        else:
            return 3

    #Daño que recibe el mago
    def damageTaken(self, damage):

        if isinstance(damage, int):
            self.__hp -= damage

    #Vuelve a dejar en false el hechizo
    def findeturno(self):
        self.__spellOk = False
