from abc import ABC
from abc import abstractmethod

class Fighter(ABC):

    @abstractmethod
    def toString():
        pass

    @abstractmethod
    def isVulnerable():
        pass

    @abstractmethod
    def damagePoints():
        pass

        
