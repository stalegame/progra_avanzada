from guerrero import Guerrero
from mago import Wizards
import random

def main():

    #Se crean los objetos
    guerrero = Guerrero()
    magito = Wizards()

    #Se crea el juego donde los personas pelean
    while guerrero.hp > 0 and magito.hp > 0:
        prob = random.randint(0,1)

        #Probabilidad de crear o no el hechizo
        if prob == 0:
            magito.prepareSpell()
        else:
            print("El ayudante no hizo el hechizo")

        #daño que recibe el guerrero
        guerrero.damageTaken(magito.damagePoints())

        #comprueba cuando daño recibira el magito
        guerrero.damagePoints(magito)

        #daño que recibe el magito
        magito.damageTaken(guerrero.damagePoints(magito))

        #para inicializar que el mago no tiene un hechizo
        magito.findeturno()
        print(f' la vida del mago es: {magito.hp} -'
              f'la vida del guerrero es: {guerrero.hp}')

if __name__ == '__main__':
    main()
