from fighter import Fighter

class Guerrero(Fighter):

    def __init__(self):
        self.__name = "Guerrero Fortinaity"
        self.__hp = 69
        self.__damage = None

    @property
    def name(self):
        return self.__name

    @property
    def hp(self):
        return self.__hp

    @property
    def damage(self):
        return self.__damage

    #Si se llama dice de que tipo es el objeto
    def toString(self):
        return 'Este objeto es de tipo <Guerrero>'

    #Devulve que el guerero no es vulnerable
    def isVulnerable(self):
        return False

    #El daño que hara al rival si este es vulnerable o no
    def damagePoints(self, other):

        if isinstance(other, Fighter):
            if other.isVulnerable():
                return 10
            else:
                return 6

    #Daño que recibe
    def damageTaken(self, damage):

        if isinstance(damage, int):
            self.__hp -= damage
