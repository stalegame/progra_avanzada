import random

class Estudiante():
    """docstring for estudiante."""

    def __init__(self, nombre):
        self._nombre = nombre
        self._asignatura = []
        self._diploma = []

    @property
    def nombre(self):
        return self._nombre

    @nombre.setter
    def nombre(self, nombre):
        if isinstance(nombre, str):
            self._nombre = nombre
        else:
            print("El tipo de dato no corresponde")

    @property
    def asignatura(self):
        return self._asignatura

    @asignatura.setter
    def asignatura(self, asignatura):
        if isinstance(asignatura, Asignatura):
            self._asignatura.append(asignatura)
        else:
            print("El tipo de dato no corresponde")

    @property
    def diploma(self):
        return self._diploma

    @diploma.setter
    def diploma(self, diploma):
        if isinstance(diploma, Diploma):
            self._diploma.append(diploma)
        else:
            print("El tipo de dato no corresponde")

    def ceder_asignatura(self, other):
        aux = random.choice(other._asignatura)
        self._asignatura.append(aux)
        other._asignatura.remove(aux)

    def cambiar_diploma(self, other, diploma1, diploma2):
        self._diploma.remove(diploma1)
        other._diploma.remove(diploma2)
        self._diploma.append(diploma2)
        other._diploma.append(diploma1)


class Asignatura():
    """docstring for Asignatura."""

    def __init__(self, nombre):
        self._nombre = nombre

    @property
    def nombre(self):
        return self._nombre


class Diploma():
    """docstring for Diploma."""

    def __init__(self, estudiante, asignatura):

        if isinstance(estudiante, Estudiante):
            self._estudiante = estudiante
        if isinstance(asignatura, Asignatura):
            self._asignatura = asignatura

    @property
    def estudiante(self):
        return self._estudiante

    @property
    def asignatura(self):
        return self._asignatura
