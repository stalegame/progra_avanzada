import random
from clases import Estudiante, Asignatura, Diploma

def main():

    Nombre = ['Thomas', 'Diego', 'Renata', 'Josefa', 'Sebastian', 'Bastian',
              'Claudio', 'Pia', 'Paula', 'Matias', 'Braulio', 'Valentina']

    Nombre_asignatura = ['Fisica', 'Programacion Avanzada', 'Calculo II',
                        'Coe', 'Mineria de datos']

    Cantidad_nombre = random.randint(6, len(Nombre))

    lista_estudiante = []
    lista_asignaturas = []
    lista_diploma = []

    #se crean los objetos estudiantes
    for x in range(0, Cantidad_nombre):
        lista_estudiante.append(Estudiante(Nombre[x]))

    #se crean los objetos asignatura
    for x in range(0, len(Nombre_asignatura)):
        lista_asignaturas.append(Asignatura(Nombre_asignatura[x]))

    #se utiliza para asignarle a cada alumno si o si una asignatura
    for item in lista_asignaturas:
        for x in range(0, 3):
            while True:
                alumno = random.choice(lista_estudiante)
                if item not in alumno.asignatura:
                    alumno.asignatura = item
                    break

    #si un estudiante se queda sin una asignatura, se la roba a otro.
    for item in lista_estudiante:
        if len(item.asignatura) == 0:
            for x in lista_estudiante:
                if len(x.asignatura) > 1:
                    item.ceder_asignatura(x)
                    break
    print('Se crean los diplomas:\n')

    for x in lista_estudiante:
        for y in x.asignatura:
            print(f'{x.nombre} {y.nombre}')
        print('***********'*3)
    print('\n\n')

    #se utiliza para crear los diplomas
    for x in lista_estudiante:
        for y in x.asignatura:
            lista_diploma.append(Diploma(x, y))

    #se utiliza para entregar los diplomas de forma desordenada
    for x in lista_estudiante:
        for y in x.asignatura:
            diploma_entregado = random.choice(lista_diploma)
            lista_diploma.remove(diploma_entregado)
            x.diploma = diploma_entregado

    print('Se entregan los diplomas a los estudiantes:\n')
    for x in lista_estudiante:
        for y in x.diploma:
            print(f'{x.nombre} recibe el diploma de {y.estudiante.nombre} de {y.asignatura.nombre}')
        print('_______________'*4)
    print('\n\n')

    #se utiliza para hacer el intercambio entre los estudiantes, hasta que
    #todos tienen el suyo propio
    for f in range(3):
        for x in range(0, len(lista_estudiante)):
            for y in lista_estudiante[x].diploma:
                if y.estudiante.nombre == lista_estudiante[x].nombre:
                    continue
                else:
                    for z in range(x+1, len(lista_estudiante)):
                        aux = 0
                        for w in lista_estudiante[z].diploma:
                            if w.estudiante.nombre == lista_estudiante[x].nombre:
                                lista_estudiante[x].cambiar_diploma(lista_estudiante[z], y, w)
                                aux = 1
                                break
                        if aux == 1:
                            break

                        for k in lista_estudiante[x].diploma:
                            temp = 0
                            if k.estudiante.nombre == lista_estudiante[x].nombre:
                                temp += 1
                        if temp == len(lista_estudiante[x].diploma):
                            break

    print('Se intercambian los diplomas entre los estudiantes:\n')
    for x in lista_estudiante:
        for y in x.diploma:
            print(f'{y.estudiante.nombre} recibe su diploma de {y.asignatura.nombre}')
        print('/////////////////'*3)
    print('\n\n')

if __name__ == '__main__':
    main()
