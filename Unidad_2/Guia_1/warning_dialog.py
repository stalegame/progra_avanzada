import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class Warning(Gtk.Dialog):
    def __init__(self):
        super().__init__(title="WARNING")
        self.add_buttons(
            Gtk.STOCK_OK, Gtk.ResponseType.OK,
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL
        )

        self.set_default_size(150, 100)

        label = Gtk.Label(label = '¿Esta seguro que desea resetear?')
        box = self.get_content_area()
        box.add(label)
        self.show_all()
