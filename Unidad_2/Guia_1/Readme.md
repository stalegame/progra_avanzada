En mi propuesta de solucion de la guia 1, sigo la propuesta dada por la guia, de crear una aplicacion.

La ventana principal hereda los atributos de Gtk.Window, en esta se crean 3 box, donde se distribuyen 2 botones y 2 entradas de texto, que se añaden a una box general.
Dentro de esta misma ventana principal, uno puede añardir tanto numeros, como texto. Esto con el fin de sumar, los numeros ingresados o los largos de la cadena de strings.

Al presionar el boton aceptar, se llama a la clase resultados_dialogo, donde se pasan los valores ingresados en las entradas de texto y ademas su suma, para mostrarle al usario el resultado final de los datos ingresados. Esta nueva ventana de dialogo, hereda de Gtk.dialog y dispone de un boton de stock y un label, que imprime la informacion al usario.

Volviendo con el segundo boton, este al presionarlo crea una ventana, que es de advertencia. La cual da a elegir entre continuar y resetear los valores de las entradas de texto o no realizar cambios.
Cuenta con dos botones, un aceptar y un cancelar, además de un label que pregunta si esta seguro de realizar la siguiente operacion. Esta ventana hereda de Gtk.dialog.
