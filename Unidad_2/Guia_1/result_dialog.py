import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class Result_dialog(Gtk.Dialog):
    def __init__(self, texto1, texto2, suma):
        super().__init__(title="Resultados")
        self.add_buttons(
            Gtk.STOCK_OK, Gtk.ResponseType.OK
        )

        self.set_default_size(150, 100)

        label = Gtk.Label(label = f'{texto1} + {texto2} su suma es {suma}')
        box = self.get_content_area()
        box.add(label)
        self.show_all()
