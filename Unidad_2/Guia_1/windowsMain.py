import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from result_dialog import Result_dialog
from warning_dialog import Warning

# la clase mainWindow hereda de Gtk.Window
class MainWindow(Gtk.Window):

    def __init__(self):
        super().__init__(title="Programa")

        # Se crean las box donde almacenaran los botones y entradas de texto
        self.box_general = Gtk.Box(orientation = Gtk.Orientation.VERTICAL,
                                   spacing = 20)
        self.add(self.box_general)

        self.box_entry = Gtk.Box(spacing = 20)
        self.box_general.pack_start(self.box_entry, True, True, 0)

        self.box_btn = Gtk.Box(spacing = 20)
        self.box_general.pack_start(self.box_btn, True, True, 0)

        # Se crean los botones, con sus respectivos metodos
        self.button1 = Gtk.Button(label="Accept")
        self.button1.connect("clicked", self.show_results)
        self.box_btn.pack_start(self.button1, True, True, 0)

        self.button2 = Gtk.Button(label="Reset")
        self.button2.connect("clicked", self.clean_entries)
        self.box_btn.pack_start(self.button2, True, True, 0)

        # Se crean las entradas de texto, con su metodo
        self.entry1 = Gtk.Entry()
        self.entry1.connect('activate', self.sum_chars)
        self.box_entry.pack_start(self.entry1,True, True,0)

        self.entry2 = Gtk.Entry()
        self.entry2.connect('activate', self.sum_chars)
        self.box_entry.pack_start(self.entry2,True, True,0)

        self.suma = 0

    # Funcion para sumar los textos si son numericos, o el largo del str si son
    # palabras
    def sum_chars(self, btn=None):

        self.text1 = self.entry1.get_text()
        self.text2 = self.entry2.get_text()

        try:
            num1 = int(self.text1)
            num2 = int(self.text2)

        except ValueError:
            num1 = len(self.text1)
            num2 = len(self.text2)

        self.suma = num1 + num2

    # Llama a la ventana de dialogo para mostrar los resultados.
    def show_results(self, btn=None):

        self.sum_chars()
        dialog = Result_dialog(self.text1, self.text2, self.suma)
        response = dialog.run()

        if response == Gtk.ResponseType.OK:
            dialog.destroy()
        elif response == Gtk.ResponseType.DELETE_EVENT:
            dialog.destroy


    # Da un mensaje de advertencia, si desea o no limpiar la info
    def clean_entries(self, btn=None):

        warning = Warning()
        response = warning.run()

        if response == Gtk.ResponseType.OK:
            self.entry1.set_text("")
            self.entry2.set_text("")
            self.suma = 0

        elif response == Gtk.ResponseType.CANCEL:
            pass
        warning.destroy()

if __name__ == '__main__':
    win = MainWindow()
    win.connect('destroy',Gtk.main_quit)
    win.show_all()
    Gtk.main()
