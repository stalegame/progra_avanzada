# Programacion Avanzada
---
# Como utilizar la aplicacion

1. Elegir el juego que desea comprar o añadir al stock.
2. Presionar el boton para ejecutar la aplicacion.
3. Ver como se compro o se añadio stock.
4. Tambien se pude presionar el boton con la imagen de estrella, para abrir una ventana adicional, la cual contiene más informacion del creador.
---
# MainWindow

Funciona como ventana principal donde se encuentra la totalidad de procesos del codigo, esta ventana hereda de Gtk.Window sus atributos.
Ademas de contener Gtk.Button, Gtk.Combobox, Gtk.HeaderBar, Gtk.Grip, Gtk.Box y Gtk.TexView.

---
# HeaderBar

Este headerbar se encarga de personalizar la ventana principal, cambiando el titulo, el subtitulo y añadiendo el boton para cerrar la ventana. adicional a este añade un boton del tipo about dialog, el cual levanta una ventana, que hereda de Gtk.AboutDialog, la cual muestra mayor informacion del codigo y del autor.

---
# TreeView

Este treeview se encarga de tomar la informacion del archivo 'Jueguitos.cvs' y procesarlo para mostrar la informacion que almacena y si es necesario cambiar o modificar dicho archivo.

---
# Buttons Add and Buy
Estos botones se encargan respectivamente de comprar(disminuir) o añadir(sumar) stock al videojuego que se eliga, estos se encargan de hacer su funcion, eliminar la linea del juego y volver a insertarla al treeview.
