import gi
import pandas
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio

name = ['Thomas Aránguiz Gaete']

class MainWindow(Gtk.Window):

    def __init__(self):
        super().__init__()

        self.set_default_size(800, 400)
        self.connect('destroy', Gtk.main_quit)

        # HeaderBar
        self.headerbar = Gtk.HeaderBar()
        self.headerbar.set_title('Pyme Jueguitos')
        self.headerbar.set_subtitle('Emporio jugabilistico')
        self.headerbar.set_show_close_button(True)
        self.set_titlebar(self.headerbar)

        # Boton about, para ingresarle una imagen
        self.btn_about = Gtk.Button()
        icon = Gio.ThemedIcon(name = 'help-about')
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.btn_about.add(image)
        self.btn_about.connect('clicked', self.on_clicked_about_btn)
        self.headerbar.pack_end(self.btn_about)

        # Se crea la grid donde se pondra el TreeView
        self.grid = Gtk.Grid()
        self.grid.set_row_spacing(20)
        self.grid.set_column_spacing(20)
        self.add(self.grid)

        # Se llama a la funcion para que cree el TreeView
        self.new_treeview()

        # Se crea una box para los botones y la combobox
        self.btn_box = Gtk.Box()
        self.combobox = Gtk.ComboBoxText()
        self.combobox.append('default', 'Elige un juego para comprar')
        self.combobox.set_active_id('default')
        self.btn_box.pack_start(self.combobox, True, True, 0)

        # Se crea el boton para comprar
        self.btn_buy = Gtk.Button(label="Comprar")
        self.btn_buy.connect('clicked', self.on_clicked_buy_btn)
        self.btn_box.pack_start(self.btn_buy, True, True, 0)

        # Se crea el boton para Añadir
        self.btn_add = Gtk.Button(label="Añadir")
        self.btn_add.connect('clicked', self.on_clicked_add_btn)
        self.btn_box.pack_start(self.btn_add, True, True, 0)

        # Se añade la box en la posicion correspondiente
        self.grid.attach(self.btn_box, 0, 0, 1, 1)

        # Se añaden los nombres de los a la combobox
        for item in self.dataframe['Juego']:
            self.combobox.append(item, item)


    def new_treeview(self):

        # En caso de que existan gran numero de filas, permite el poder bajar la ventana
        scrolled_window = Gtk.ScrolledWindow()
        scrolled_window.set_hexpand(True)
        scrolled_window.set_vexpand(True)
        self.grid.attach(scrolled_window, 0, 1, 1, 1)
        self.treeview = Gtk.TreeView()
        scrolled_window.add(self.treeview)

        # Se carga la data del archivo jueguitos
        self.dataframe = pandas.read_csv('jueguitos.csv')

        # Si se vuelve a llamar la funcion se eliminan las columnas y se cambian por las nuevas
        if self.treeview.get_columns():
            for column in self.treeview.get_columns():
                self.treeview.remove_column(column)

        # Se crea el modelo del treeview
        len_columns = len(self.dataframe.columns)
        model = Gtk.ListStore(*(len_columns * [str]))
        self.treeview.set_model(model=model)

        cell = Gtk.CellRendererText()

        # Se añaden los valores a las columnas y filas
        for item in range(len_columns):
            column = Gtk.TreeViewColumn(self.dataframe.columns[item], cell, text=item)
            self.treeview.append_column(column)
            column.set_sort_column_id(item)

        for item in self.dataframe.values:
            line = [str(x) for x in item]
            model.append(line)

        self.show_all()


    #Se abre la ventana de dialogo tipo about
    def on_clicked_about_btn(self, btn = None):
        about_dialog = AboutDialog()


    def on_clicked_buy_btn(self, btn = None):

        lista = ['Juego', 'Valoracion', 'Precio' ,'Stock']
        aux = self.combobox.get_active_text()
        # Se utiliza para sacar el valor de la posicion del juego que uno elige
        index = self.dataframe[self.dataframe['Juego'] == aux].index.tolist()

        for row in self.dataframe.values:

            # Se hace la lista por comprension, que devulve el valor de stock
            if aux in row:
                temp = [item for item in row]

                """ Se utiliza el siguiente if, para remover la lista del juego el cual uno Elige
                    y luego volverla a añadirla, pero con el stock -1.
                    y ademas de ello elimina si se crean más comunas de las necesarias"""
                if temp[-1] > 0:
                    temp[-1] -= 1
                    self.dataframe = self.dataframe.drop(index)
                    dictionary = {'Juego': temp[0], 'Valoracion': temp[1], 'Precio': temp[2], 'Stock': temp[3]}
                    self.dataframe = self.dataframe.append(dictionary, ignore_index=True)

                    for j in self.dataframe.columns:
                        if j not in lista:
                            del self.dataframe[f'{j}']
                    self.dataframe.to_csv('jueguitos.csv', index = False)
                    self.new_treeview()
                break

    # Esta funcion hace lo mismo que la de arriba pero le suma 1 al stock
    def on_clicked_add_btn(self, btn = None):
        lista = ['Juego', 'Valoracion', 'Precio' ,'Stock']
        aux = self.combobox.get_active_text()
        x = self.dataframe[self.dataframe['Juego'] == aux].index.tolist()
        for row in self.dataframe.values:

            if aux in row:
                temp = [item for item in row]
                temp[-1] += 1

                self.dataframe = self.dataframe.drop(x)
                dictionary = {'Juego': temp[0], 'Valoracion': temp[1], 'Precio': temp[2], 'Stock': temp[3]}
                self.dataframe = self.dataframe.append(dictionary, ignore_index=True)

                for j in self.dataframe.columns:
                    if j not in lista:
                        del self.dataframe[f'{j}']
                self.dataframe.to_csv('jueguitos.csv', index = False)
                self.new_treeview()
                break


class AboutDialog(Gtk.AboutDialog):

    def __init__(self):
        super().__init__(title = 'About Progra')
        self.set_modal(True)

        # Se crean las caracteristicas de la about dialog
        self.add_credit_section('Autor:', name)
        self.set_comments('Tercera guia unidad 2 - Programacion Avanzada')
        self.set_logo_icon_name('applications-internet')
        self.set_program_name('Pyme Jueguitos')
        self.set_version('version 1.0')

        self.show_all()


if __name__ == '__main__':
    win = MainWindow()
    win.connect('destroy',Gtk.main_quit)
    win.show_all()
    Gtk.main()
