import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

def on_info_clicked(widget, principal, secundario):

    dialog = Gtk.MessageDialog(widget, 0, Gtk.MessageType.INFO,
                               Gtk.ButtonsType.OK, principal)

    dialog.format_secondary_text(secundario)
    dialog.run()

    dialog.destroy()


def on_warning_clicked(widget, principal, secundario):

    dialog = Gtk.MessageDialog(widget, 0, Gtk.MessageType.WARNING,
                               Gtk.ButtonsType.YES_NO, principal)

    dialog.format_secondary_text(secundario)
    response = dialog.run()

    if response == Gtk.ResponseType.YES:
        response = True
    elif response == Gtk.ResponseType.NO:
        response = False

    dialog.destroy()
    return response
