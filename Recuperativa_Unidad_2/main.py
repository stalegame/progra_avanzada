import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from message_dialog import on_info_clicked, on_warning_clicked

class MainWindow(Gtk.Window):

    def __init__(self):
        super().__init__()

        self.maximize()
        self.set_resizable(False)
        self.connect('destroy', Gtk.main_quit)

        # HeaderBar
        self.headerbar = Gtk.HeaderBar()
        self.headerbar.set_title('Recuperativa')
        self.headerbar.set_show_close_button(True)
        self.set_titlebar(self.headerbar)

        # Se crea la grid
        self.grid = Gtk.Grid()
        self.grid.set_row_spacing(20)
        self.grid.set_column_spacing(20)
        self.add(self.grid)

        # entry
        self.entry_1 = Gtk.Entry()
        self.entry_1.connect('changed', self.sumar)
        self.grid.attach(self.entry_1, 1, 0, 1, 1)

        self.entry_2 = Gtk.Entry()
        self.entry_2.connect('changed', self.sumar)
        self.grid.attach(self.entry_2, 1, 1, 1, 1)

        # label
        self.label_1 = Gtk.Label(label = 'texto 1: ')
        self.grid.attach(self.label_1, 0, 0, 1, 1)

        self.label_2 = Gtk.Label(label = 'texto 2: ')
        self.grid.attach(self.label_2, 0, 1, 1, 1)

        self.label_3 = Gtk.Label(label = 'La suma es: 0')
        self.grid.attach(self.label_3, 1, 2, 1, 1)

        # btns
        self.btn_ok = Gtk.Button(label = 'aceptar')
        self.btn_ok.connect('clicked', self.info_clicked)

        self.grid.attach(self.btn_ok, 0, 3, 1, 1)

        self.btn_save = Gtk.Button(label = 'guardar')
        self.btn_save.connect('clicked', self.filechooserdialog)
        self.grid.attach(self.btn_save, 1, 3, 1, 1)

        self.btn_reset = Gtk.Button(label = 'resetear')
        self.btn_reset.connect('clicked', self.reset_clicked)
        self.grid.attach(self.btn_reset, 2, 3, 1, 1)

        self.suma = 0


    def sumar(self, enter):

        self.text1 = self.entry_1.get_text().replace(' ', '').strip(' ')
        self.text2 = self.entry_2.get_text().replace(' ', '').strip(' ')
        contador = 0


        if self.text1.isnumeric() and self.text2.isnumeric():
            self.suma = int(self.text1) + int(self.text2)

        elif self.text1.isnumeric() and self.text2.isnumeric() is False:

            for letra in self.text2:
                if letra.lower() in "aeiou":
                    contador += 1

            self.suma = int(self.text1) + contador

        elif self.text2.isnumeric() and self.text1.isnumeric() is False:

            for letra in self.text1:
                if letra.lower() in "aeiou":
                    contador += 1

            self.suma = int(self.text2) + contador

        else:

            for letra in self.text1:
                if letra.lower() in "aeiou":
                    contador += 1

            for letra in self.text2:
                if letra.lower() in "aeiou":
                    contador += 1

            self.suma = contador

        label_tmp = f'La suma es: {self.suma}'
        self.label_3.set_text(label_tmp)


    def info_clicked(self, btn = None):

        self.text1 = self.entry_1.get_text().replace(' ', '').strip(' ')
        self.text2 = self.entry_2.get_text().replace(' ', '').strip(' ')

        principal = 'Resultado del procedimiento'
        secundario = f'{self.text1} más {self.text2} \nsuman {self.suma}'

        on_info_clicked(self, principal, secundario)


    def reset_clicked(self, btn = None):

        principal = 'Reseteo de informacion'
        secundario = '¿Esta seguro que desea reiniciar las entradas?'

        if not on_warning_clicked(self, principal, secundario):
            return

        self.entry_1.set_text('')
        self.entry_2.set_text('')
        cosa = f'La suma es: 0'
        self.label_3.set_text(cosa)


    def filechooserdialog(self, btn=None):
        
        f_dialog = Gtk.FileChooserDialog(
            title = 'Elegir donde guardar el archivo', parent=self, action=Gtk.FileChooserAction.SAVE
        )
        f_dialog.add_buttons(
            Gtk.STOCK_CANCEL,
            Gtk.ResponseType.CANCEL,
            Gtk.STOCK_SAVE,
            Gtk.ResponseType.OK
        )
        f_dialog.set_current_name(f'Pavanzada.txt')

        response = f_dialog.run()

        if response == Gtk.ResponseType.OK:
            self.guardar(f_dialog.get_filename())

        f_dialog.destroy()


    def guardar(self, path):

        with open(path, 'a') as archivo:
            archivo.write(self.label_3.get_text() + '\n')


if __name__ == '__main__':
    win = MainWindow()
    win.connect('destroy', Gtk.main_quit)
    win.show_all()
    Gtk.main()
