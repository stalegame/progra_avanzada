from enfermedad import Enfermedad

class Persona():
    def __init__(self, run, comunidad):
        self.__comunidad = comunidad
        self.__run = run
        self.__familia = []
        self.__enfermedad = None
        self.__estado = True


    @property
    def comunidad(self):
        return self.__comunidad

    @comunidad.setter
    def comunidad(self, comunidad):
        if isinstance(comunidad, str):
            self.__comunidad = comunidad
        else:
            print("El tipo de dato no corresponde")


    @property
    def run(self):
        return self.__run

    @run.setter
    def run(self, run):
        if isinstance(run, int):
            self.__run = run
        else:
            print("El tipo de dato no corresponde")


    @property
    def familia(self):
        return self.__familia

    @familia.setter
    def familia(self, familiar):
        if isinstance(familiar, Persona):
            self.__familia.append(familiar)
        else:
            print("El tipo de dato no corresponde")


    @property
    def enfermedad(self):
        return self.__enfermedad

    @enfermedad.setter
    def enfermedad(self, virus):
        if isinstance(virus, Enfermedad):
            self.__enfermedad = virus
        else:
            print("El tipo de dato no corresponde")


    @property
    def estado(self):
        return self.__estado
