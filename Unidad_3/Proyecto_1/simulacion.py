from persona import Persona
from comunidad import Comunidad
import random as r

class Simulacion():
    def __init__(self, comunidad):

        self.__comunidad = comunidad

    def crear_comunidad(self):

        for x in range(self.__comunidad.num_ciu):
            persona = Persona(x, self.__comunidad)
            self.__comunidad.list_cui = persona

        r.shuffle(self.__comunidad.list_cui)
        for x in self.__comunidad.list_cui:
            print(x.run)

        lista = []
        for x in self.__comunidad.list_cui:
            lista.append(x)
            if len(lista) == 5:
                for x in self.__comunidad.list_cui:
                    if x in lista:
                        for h in lista:
                            if x != h:
                                x.familia = h
                lista = []

        for x in self.__comunidad.list_cui:
            print('--'*10)
            print(f'familia de {x.run}')
            for hh in x.familia:
                print(hh.run)

        wea = r.sample(self.__comunidad.list_cui, self.__comunidad.num_infe)

        for x in wea:
            print(x.run)
            print(x.enfermedad)
