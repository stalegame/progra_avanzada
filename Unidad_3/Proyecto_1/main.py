from enfermedad import Enfermedad
from comunidad import Comunidad
from simulacion import Simulacion

def main():

    covid = Enfermedad(infeccion_probable=0.3,
                       promedio_pasos=18)

    talca = Comunidad(num_ciudadanos=50,
                      promdeio_conexion_fisica=8,
                      enfermedad=covid,
                      num_infectados=10,
                      probabilidad_conexion_fisica=0.8)

    sim = Simulacion(talca)
    sim.crear_comunidad()

if __name__ == '__main__':
    main()
