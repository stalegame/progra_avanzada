from alumnos import Alumnos
from plant import Plant, Hierbas, Trebol, Rabanos, Violetas
import random


class Garden():
    def __init__(self):
        self._alumnos = []

    @property
    def alumnos(self):
        return self._alumnos

    @alumnos.setter
    def alumnos(self, alumnos):
        if isinstance(alumnos, Alumnos):
            self._alumnos.append(alumnos)
        else:
            print("El tipo de dato no corresponde")

    def repartir_plantas(self):

        for x in self._alumnos:
            for y in range(4):
                aux = random.randint(1,4)
                if aux == 1:
                    planta = Hierbas()

                elif aux == 2:
                    planta = Trebol()

                elif aux == 3:
                    planta = Rabanos()

                elif aux == 4:
                    planta = Violetas()

                x.plantas = planta

    def especifica(self, alumno):
        fila_1 = ""
        fila_2 = ""
        if isinstance(alumno, Alumnos):
            for x in self._alumnos:
                tem = x.plantas

                if x.nombre == alumno.nombre:
                    for y in range(0,2):

                        fila_1 += tem[y].nombre[0]
                fila_1 += '--'

                if x.nombre == alumno.nombre:
                    for z in range(2,4):
                        fila_2 += tem[z].nombre[0]
                fila_2 += '--'

            fila_1 = fila_1[:-2]
            fila_2 = fila_2[:-2]
            print('\n')
            print('[Ventana]'*3)
            print(fila_1)
            print(fila_2)
            print('\n')
