from garden import Garden
from alumnos import Alumnos
from plant import Plant

# Se implementa la funcion para ordenar y mostrar la informacion al usario
def imprimir_data(jardin):
    fila_1 = ""
    fila_2 = ""

    # Se recorren los alumnos y sus listas de plantas, para separarlos
    # y mostrarlos como se pide en la guia
    for x in jardin.alumnos:
        tem = x.plantas

        for y in range(0,2):
            fila_1 += tem[y].nombre[0]
        fila_1 += '-'
        for z in range(2,4):
            fila_2 += tem[z].nombre[0]
        fila_2 += '-'

    fila_1 = fila_1[:-1]
    fila_2 = fila_2[:-1]
    print('[Ventana]'*3)
    print(fila_1)
    print(fila_2)
    print('\n')

# Funcion principal donde se llamaran a las clases y funciones
def main():

    # Se implementa la lista de alumnos y se ordenan con el sort
    # de forma alfabetica
    lista_nombre = ['Alicia', 'Marit', 'Pepito', 'David', 'Eva', 'Lucia',
                     'Rocı́o', 'Andrés', 'José', 'Belen', 'Sergio', 'Larry']
    lista_nombre.sort()

    # Se crea el objeto de tipo Garden
    jardin = Garden()

    # Se crean los objetos de tipo Estudiantes
    for x in lista_nombre:
        jardin.alumnos = Alumnos(x)

    print("Alumnos del jardin:\n")
    for x in jardin.alumnos:
        print(x.nombre)
    print('\n')

    # Se llama al metodo, para crear las plantas para cada Alumno dentro
    # de Garden, y estas mismas plantas son objetos del tipo Plant
    jardin.repartir_plantas()

    # Se implementa el menu para que el usario pueda elegir que desea hacer
    opciones = -1
    while opciones!= 0:
        print('Menu de interaccion')
        print('< 1 > Mostrar Informacion del jardin')
        print('< 2 > Mostar info especifica de alumno')
        print('< 0 > Finalizar ejecucion\n')
        opciones = int(input('Ingrese el numero que desee utilizar: '))

        # Se muestra la informacion completa de alumnos y sus plantas
        # en cuestion
        if opciones == 1:
            print('\n')
            imprimir_data(jardin)

        # Primero da a elegir el alumno, que se quiera saber sus plantas y
        # luego llama al metodo para imprimir unicamente la info del estudiante
        # elegido
        elif opciones == 2:
            y = 1
            print('\n')
            for x in jardin.alumnos:
                print(f' <{y}> {x.nombre}')
                y += 1
            print('\n')
            num = int(input('Ingrese el numero del alumno que quiere ver sus'
                            'plantas: '))
            jardin.especifica(jardin.alumnos[num-1])

        # Se cancela el ciclo, y culmina la ejecucion
        elif  opciones == 0:
            print('Gracias por utilizar el programa')


if __name__ == '__main__':
    main()
