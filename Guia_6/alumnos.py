from plant import Plant

class Alumnos():
    def __init__(self, nombre):
        self._nombre = nombre
        self._plantas = []

    @property
    def nombre(self):
        return self._nombre

    @nombre.setter
    def nombre(self, nombre):
        if isinstance(nombre, str):
            self._nombre = nombre
        else:
            print("El tipo de dato no corresponde")

    @property
    def plantas(self):
        return self._plantas

    @plantas.setter
    def plantas(self, plantas):
        if isinstance(plantas, Plant):
            self._plantas.append(plantas)
        else:
            print("El tipo de dato no corresponde")
