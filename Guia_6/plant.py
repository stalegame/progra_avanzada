class Plant():
    def __init__(self):
        self._nombre = None

    @property
    def nombre(self):
        return self._nombre


class Hierbas(Plant):
    def __init__(self):
        super().__init__()
        self._nombre = 'Hierbas'


class Trebol(Plant):
    def __init__(self):
        super().__init__()
        self._nombre = 'Trebol'


class Rabanos(Plant):
    def __init__(self):
        super().__init__()
        self._nombre = 'Rabanos'


class Violetas(Plant):
    def __init__(self):
        super().__init__()
        self._nombre = 'Violetas'
